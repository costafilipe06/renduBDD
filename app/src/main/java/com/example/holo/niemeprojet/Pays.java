package com.example.holo.niemeprojet;

import io.realm.RealmObject;

/**
 * Created by Holo on 19/12/2017.
 */

public class Pays extends RealmObject {
    private String nom;
    private String imgUrl;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Pays(){}

    public Pays(String _nom, String _imgUrl){
        nom = _nom;
        imgUrl = _imgUrl;
    }
    @Override
    public String toString(){
        return "- " + nom + " (" + imgUrl +")";
    }
}
