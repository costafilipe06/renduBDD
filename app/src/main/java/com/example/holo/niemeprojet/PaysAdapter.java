package com.example.holo.niemeprojet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by Holo on 21/12/2017.
 */

public class PaysAdapter extends ArrayAdapter<Pays> {

    public PaysAdapter(Context context, List<Pays> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Pays pays = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.my_item_layout, parent, false);
        }

        TextView nomPays = (TextView) convertView.findViewById(R.id.NomPays);
        WebView drapeauPays = (WebView) convertView.findViewById(R.id.flag);


        nomPays.setText(pays.getNom());
        drapeauPays.loadUrl(pays.getImgUrl());


        return convertView;
    }
}
