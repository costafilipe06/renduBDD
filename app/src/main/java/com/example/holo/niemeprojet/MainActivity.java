package com.example.holo.niemeprojet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    List<Pays> BD = new ArrayList<>();
    static Realm realm;

    public static String url = "https://restcountries.eu/rest/v2/all?fields=name;capital;flag;alpha2Code";
    public static Context context;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this.getApplicationContext();
        Realm.init(this);

        // Create the Realm instance
        realm = Realm.getDefaultInstance();

        Button getJSON = (Button) findViewById(R.id.GetJSON_Button);

        getJSON.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                new JsonTask().execute(url);
            }
        });

        Button printAllPays = (Button) findViewById(R.id.GetDB_Button);
        printAllPays.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                PrintTousLesPaysDeRealm(realm);
            }
        });

        Button emptyBDD = (Button) findViewById(R.id.EmptyBDD_Button);
        emptyBDD.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                ViderLaBDD(realm);
            }
        });

        Button dansListe = (Button) findViewById(R.id.show_in_list);
        dansListe.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this, PaysListActivity.class);
                startActivity(intent);
            }
        });
    }


    private class JsonTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(MainActivity.this);
            pd.setMessage("Please wait");
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... params) {


            HttpURLConnection connection = null;
            BufferedReader reader = null;



            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();


                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)

                }

                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pd.isShowing()){
                pd.dismiss();
            }

            try {
                System.out.println("\n\n\n@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n\n");
                JSONArray jsonObject = new JSONArray(result);

                Log.i("Get", "Resetting local BD");
                BD.removeAll(BD);


                for(int i=0; i<jsonObject.length(); i++){

                    JSONObject paysObj = new JSONObject(jsonObject.getString(i));

                    BD.add(new Pays(paysObj.get("name").toString(), paysObj.get("flag").toString()));
                    //Preuve que c'est dans la liste
                    System.out.println(BD.get(i).toString());

                    AjouterPaysBDD(realm, BD.get(i));
                }
                Log.i("Done", "All Countries added.");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            System.out.println("\n\n\n@@@@@@@@@@@@@ fin @@@@@@@@@@@@@\n\n\n");

            CharSequence text = "Tous les Pays ont été ajoutés à la BDD Realm.";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }
    }

    private void AjouterPaysBDD(Realm realm, final Pays _pays){
        // All writes must be wrapped in a transaction to facilitate safe multi threading
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // Add a person
                Pays pays = realm.createObject(Pays.class);
                pays.setNom(_pays.getNom());
                pays.setImgUrl(_pays.getImgUrl());
            }
        });
    }

    private void PrintTousLesPaysDeRealm(Realm realm){
        final List<Pays> PaysAuPluriel = realm.where(Pays.class).findAll();
        Log.i("Nombre total de Pays", Integer.toString(PaysAuPluriel.size()));

        if(PaysAuPluriel.size() < 1){
            CharSequence text = "La BDD Realm est vide. Cliquez sur le premier bouton.";
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

            return;
        }

        Toast toast = Toast.makeText(context, "Il y a " + Integer.toString(PaysAuPluriel.size()) + " Pays dans la BDD", Toast.LENGTH_LONG);
        toast.show();

        for(Pays p : PaysAuPluriel){
            Log.i("Pays", p.getNom() + " --> " + p.getImgUrl());
        }
    }

    private void ViderLaBDD(Realm realm){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Pays> PaysAuPluriel = realm.where(Pays.class).findAll();
                PaysAuPluriel.deleteAllFromRealm();
            }
        });

        Log.i("Vider la BDD", "Toutes les Pays ont été supprimés.");


        CharSequence text = "Tous les Pays ont été supprimés.";
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}
