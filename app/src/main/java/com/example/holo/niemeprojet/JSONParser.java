package com.example.holo.niemeprojet;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Objects;

/**
 * Created by Holo on 19/12/2017.
 */


public class JSONParser {

    String stringToParse = "";

    // constructor
    public JSONParser(String _toParse) {
        stringToParse = _toParse;
    }

    public void ParsingString(){
        try {
            JSONObject jsonObject = new JSONObject(stringToParse);
            Iterator<String> iter = jsonObject.keys();
            while(iter.hasNext()){
                String key = iter.next();
                System.out.println("- " + jsonObject.get(key));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
