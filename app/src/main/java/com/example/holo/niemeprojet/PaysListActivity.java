package com.example.holo.niemeprojet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import java.util.List;

import io.realm.Realm;

public class PaysListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pays_list);

        Realm realm = MainActivity.realm;
        final List<Pays> PaysAuPluriel = realm.where(Pays.class).findAll();

        PaysAdapter paysAdapter = new PaysAdapter(this, PaysAuPluriel);

        ListView listView = (ListView) findViewById(R.id.PaysList);
        listView.setAdapter(paysAdapter);

        Log.i("Adapter", "Done");

    }
}
